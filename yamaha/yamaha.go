package yamaha

import (
  "net/http"
  "strings"
  "fmt"
)

func sendcmd(command string) {
  body := strings.NewReader(fmt.Sprintf(`<YAMAHA_AV cmd="PUT"><Main_Zone><Power_Control><Power>%s</Power></Power_Control></Main_Zone></YAMAHA_AV>`, command))
  req, err := http.NewRequest("POST", "http://192.168.1.74/YamahaRemoteControl/ctrl", body)
  if err != nil {
	// handle err
	println("error!")
  }
  req.Header.Set("Content-Type", "text/xml; charset=UTF-8")

  resp, err := http.DefaultClient.Do(req)
  if err != nil {
	// handle err
        println(resp.StatusCode)
	println(err)
  }
  defer resp.Body.Close()
}

func Off() {
  sendcmd("Standby")
}

func On() {
  sendcmd("On")
}

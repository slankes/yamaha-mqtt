package main

import (
  "fmt"
  //import the Paho Go MQTT library
  MQTT "github.com/eclipse/paho.mqtt.golang"
  "os"
  "time"
  "./yamaha"
)

//define a function for the default message handler
var f MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
  fmt.Printf("TOPIC: %s\n", msg.Topic())
  fmt.Printf("MSG: %s\n", msg.Payload())
  if msg.Topic() == "cmnd/yamaha/POWER" {
    if string(msg.Payload()) == "ON" {
      yamaha.On()
      fmt.Printf("On")
    } else {
      yamaha.Off()
      fmt.Printf("Off")
    }
  }
}

func connLostHandler(c MQTT.Client, err error) {
  fmt.Printf("Connection lost, reason: %v\n", err)
  //Perform additional action...
}

func main() {
  //create a ClientOptions struct setting the broker address, clientid, turn
  //off trace output and set the default message handler
  opts := MQTT.NewClientOptions().AddBroker("tcp://192.168.1.2:1883")
  opts.SetClientID("go-simple")
  opts.SetDefaultPublishHandler(f)
  opts.SetConnectionLostHandler(connLostHandler)

  //set OnConnect handler as anonymous function
  //after connected, subscribe to topic
  opts.OnConnect = func(c MQTT.Client) {
    fmt.Printf("Client connected, subscribing to: cmnd/yamaha/POWER\n")
    //Subscribe here, otherwise after connection lost, 
    //you may not receive any message
    if token := c.Subscribe("cmnd/yamaha/POWER", 0, nil); token.Wait() && token.Error() != nil {
      fmt.Println(token.Error())
      os.Exit(1)
    }
  }

  //create and start a client using the above ClientOptions
  c := MQTT.NewClient(opts)
  if token := c.Connect(); token.Wait() && token.Error() != nil {
    panic(token.Error())
  }

  for {
      //Lazy...
      time.Sleep(500 * time.Millisecond)
  }

}
